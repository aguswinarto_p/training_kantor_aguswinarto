<?php

class Article extends CI_Model {

  public function add(){	
	$username = $this->session->userdata('username');
    $data = array(
	  'title' => $this->input->post('title'),
	  'content' => $this->input->post('content'),
	  'username' => $username
	);	
	$insert = $this->db->insert('article', $data);
	return $insert;
  }
	
  public function get_all(){
	$query=$this->db->get("article"); 
	Return $query->result(); 
  }

  public function view($num, $offset){
	$query=$this->db->get("article", $num, $offset); 
	Return $query->result(); 
  }

  public function detail($num, $offset, $id_article){
	$this->db->where('id_article',$id_article); 
	$query=$this->db->get("comment", $num, $offset); 
	Return $query->result(); 
  }

  public function per_id($id_article) { 
	$this->db->where('id_article',$id_article); 
	$query=$this->db->get('article'); 
	return $query->result(); 
  }
	
  public function edit() { 
	$id_article=$this->input->post('id_article'); 
	$username = $this->session->userdata('username');
	$data_article=array( 
	  'title' => $this->input->post('title'),			
	  'content' => $this->input->post('content'),			
	  'username' => $username
	); 
	$this->db->where('id_article',$id_article); 
	$update=$this->db->update('article',$data_article); 
	return $update;  
  }	

  public function delete($id_article) { 
	$this->db->where('id_article',$id_article); 
	$hapus=$this->db->delete('article'); 
	return $hapus; 
  }

  public function comment($id_article){	
	$username = $this->session->userdata('username');
    $data = array(
	  'member' => "Y",
	  'name' => $username,
	  'id_article' => $id_article,
	  'comment' => $this->input->post('content'),
	  'username' => $username
	);	
	$insert = $this->db->insert('comment', $data);
	return $insert;
  }

}