﻿<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="ie6 ielt8"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="ie7 ielt8"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html lang="en"> <!--<![endif]-->
<head>
<?php 
	$data['title'] = "Login Form";
	$this->load->view('Users/includes/header',$data); ?>
</head>
<body>
<script language="javascript">
alert('<?=$message; ?>');
</script>
<div class="container">
  <section id="content">
    <?php echo form_open_multipart('Users/login'); ?>
    <h1>Login Form</h1>
    <div><input type="text" placeholder="Username" name="username" id="username"/></div>
    <div class="error"><?php echo form_error('username'); ?></div>
    <div><input type="password" placeholder="Password" name="password" id="password"/></div>
    <div class="error"><?php echo form_error('password'); ?></div>
    <div><input type="submit" value="Log in" />
      <a href="<?php echo base_url();?>Users/register/">Register</a>
    </div>
  <?php	echo form_close(); ?>
  </section>
</div>
</body>
</html>
