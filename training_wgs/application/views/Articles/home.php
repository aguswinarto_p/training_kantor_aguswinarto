<!DOCTYPE html>
<html lang="en">

<head>
<?php $this->load->view('Articles/includes/header'); ?>
</head>

<body>
  <div id="container">
    <div id="out-wraper">
	  <?php $this->load->view('Articles/includes/title'); ?>
	  <?php $this->load->view('Articles/includes/navigation'); ?>
      <div class="content">
	    <div class="left_content">
	      <?php $this->load->view('Articles/includes/left_content'); ?>
        </div>
	    <div class="right_aside">
	      <?php $this->load->view('Articles/includes/right_content'); ?>
        </div>
      </div>
    </div>
    <div class="clear"></div>
  </div>
  <div id="container">
  <div id="footer">
    <?php $this->load->view('Articles/includes/footer'); ?>
    <?php $this->load->view('Articles/includes/copyright'); ?>
  </div>
    <div class="clear"></div>
  </div>
</body>
</html>