<!DOCTYPE html>
<html lang="en">

<head>
<?php $this->load->view('Articles/includes/header'); ?>
</head>

<body>
  <div id="container">
    <div id="out-wraper">
	  <?php $this->load->view('Articles/includes/title'); ?>
	  <?php $this->load->view('Articles/includes/navigation'); ?>
      <div class="content">
	    <div class="left_content">
          <?Php foreach($data as $content){ ?>
          <article>
            <header>
              <div class="date_section">
			    <div class="year">2014</div>
			    <div class="date">9</div>
			    <div class="month">Jan</div>
              </div>
              <div class="title_article">
                <?=$content->title; ?>
              </div>
              <div class="comment">
                10
              </div>
            </header>
		    <div class="content">
		      <p><?=$content->content; ?></p>	
            </ div>
            <section>
              <div class="tag">
                <a href="<?=base_url(); ?>Articles/detail/<?=$content->id_article; ?>">Detail</a>
                <a href="<?=base_url(); ?>Articles/edit/<?=$content->id_article; ?>">Edit</a>
                <a href="<?=base_url(); ?>Articles/delete/<?=$content->id_article; ?>">Delete</a>
              </div>
            </section>
          </article>
          <?php } ?>
          <?=$page; ?>
        </div>
	    <div class="right_aside">
	      <?php $this->load->view('Articles/includes/right_content'); ?>
        </div>
      </div>
    </div>
    <div class="clear"></div>
  </div>
  <div id="container">
  <div id="footer">
    <?php $this->load->view('Articles/includes/footer'); ?>
    <?php $this->load->view('Articles/includes/copyright'); ?>
  </div>
    <div class="clear"></div>
  </div>
</body>
</html>