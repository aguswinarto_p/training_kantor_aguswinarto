<?php defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'/libraries/REST_Controller.php';

class API_Articles extends REST_Controller{

  function articles_get(){
	$this->load->model('Article');	
    $articles = $this->Article->get_all();
    if($articles){
     $this->response($articles, 200); // 200 being the HTTP response code
    } else {
      $this->response(array('error' => 'Couldn\'t find any articles!'), 404);
    }
  }

}