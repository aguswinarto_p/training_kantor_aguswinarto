<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller {

  public function __construct(){
    parent::__construct();
	$this->load->model('User');	
  }

  public function index(){
	if($this->check_login()){
	  $this->load->view('Articles/home');		
	} else {
	  $this->load->view('Users/login');
	}
  }

  public function login(){		
	$this->load->library('form_validation');			
	$this->form_validation->set_rules('username', 'Username', 'trim|required');
	$this->form_validation->set_rules('password', 'Password', 'trim|required');
	if(($this->form_validation->run() == FALSE)){
	  $this->load->view('Users/login');
	} else {			
	  $query = $this->User->login();
	  if($query) {
	    $data = array(
	    'username' => $this->input->post('username'),
	    'login_status' => true
	    );
	    $this->session->set_userdata($data);
	    redirect('');
	  } else if($this->input->post('username')){
	      $data['message']="Invalid Username or password !";
	      $this->load->view('Users/login',$data);	  
	  }
	}
  }	

  public function register(){
	if($this->check_login()){
	  $this->load->view('Articles/home');		
	} else {
      $this->load->helper('captcha'); 
      $vals = array(
        'img_path'	 => './captcha/',
        'img_url'	 => base_url().'captcha/',
        'img_width'	 => '200',
        'img_height' => 30,
        'border' => 0, 
        'expiration' => 7200
      );
      // create captcha image
      $cap = create_captcha($vals);
      // store image html code in a variable
      $data['image'] = $cap['image'];
      // store the captcha word in a session
      $this->session->set_userdata('captcha', $cap['word']);
	  if(($this->input->post('token') != $this->session->userdata('captcha')) && $this->input->post()){
		$data['captcha'] = "Captcha invalid !";
	  } else {
	  	$data['captcha'] = "";
	  }
      $this->load->view('Users/register', $data);
	}
  }

  public function add(){
	$this->load->library('form_validation');
			
	$this->form_validation->set_rules('name', 'Full Name', 'trim|required');
	$this->form_validation->set_rules('username', 'Username', 'trim|required');
	$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
	$this->form_validation->set_rules('password', 'Password', 'trim|required');

	if(($this->form_validation->run() == FALSE) || $this->input->post('token') != $this->session->userdata('captcha')){
	  $this->register();
	} else {
	  if($query = $this->User->add()) {
	    $data['message']="Register success !";
	    $this->load->view('Users/login',$data);	  
	  } else {
        $this->register();
	  }
	}		
  }

  public function logout(){
	$this->session->sess_destroy();
	redirect('');
  }

  private function check_login(){
	$login_status = $this->session->userdata('login_status');
	if(!isset($login_status) || $login_status != true){
	  return false;
	} else {
	  return true;
	}  
  }
}