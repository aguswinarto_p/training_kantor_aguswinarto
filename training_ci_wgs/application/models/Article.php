<?php

class Article extends CI_Model {

  public function add(){	
    $data = array(
	  'title' => $this->input->post('title'),
	  'content' => $this->input->post('content')
	);	
	$insert = $this->db->insert('article', $data);
	return $insert;
  }
	
  public function view($num, $offset){
	$query=$this->db->get("article", $num, $offset); 
	Return $query->result(); 
  }

  public function per_id($id_article) { 
	$this->db->where('id_article',$id_article); 
	$query=$this->db->get('article'); 
	return $query->result(); 
  }
	
  public function edit() { 
	$id_article=$this->input->post('id_article'); 
	$data_article=array( 
	  'title' => $this->input->post('title'),			
	  'content' => $this->input->post('content')			
	); 
	$this->db->where('id_article',$id_article); 
	$update=$this->db->update('article',$data_article); 
	return $update;  
  }	

  public function delete($id_article) { 
	$this->db->where('id_article',$id_article); 
	$hapus=$this->db->delete('article'); 
	return $hapus; 
  }

}