<!DOCTYPE html>
<html lang="en">

<head>
<?php $this->load->view('Articles/includes/header'); ?>
</head>

<body>
  <div id="container">
    <div id="out-wraper">
	  <?php $this->load->view('Articles/includes/title'); ?>
	  <?php $this->load->view('Articles/includes/navigation'); ?>
      <div class="content">
	    <div class="left_content">
          <article>
            <header>
              <div class="title_article">
                ADD NEW AN ARTICLE
              </div>
            </header>
		    <div class="content">
			  <?php 	echo form_open_multipart('Articles/add'); ?>
			  <div class="controls">
			    <?php echo validation_errors('<p class="error">'); ?>
			  </div>
		      <p> Title Article <input type="text" name="title" class="text"></p>
              <p> Content Aricle <textarea name="content" class="textarea"></textarea></p>	
			  <p> <input type="submit" class="more" value="Add"></p>
			  <?php	echo form_close(); ?>
            </div>
          </article>
        </div>
	    <div class="right_aside">
	      <?php $this->load->view('Articles/includes/right_content'); ?>
        </div>
      </div>
    </div>
    <div class="clear"></div>
  </div>
  <div id="container">
  <div id="footer">
    <?php $this->load->view('Articles/includes/footer'); ?>
    <?php $this->load->view('Articles/includes/copyright'); ?>
  </div>
    <div class="clear"></div>
  </div>
</body>
</html>