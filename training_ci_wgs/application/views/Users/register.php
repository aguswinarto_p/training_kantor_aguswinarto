﻿<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="ie6 ielt8"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="ie7 ielt8"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html lang="en"> <!--<![endif]-->
<head>
<?php 
	$data['title'] = "Register Form";
	$this->load->view('Users/includes/header',$data); ?>
</head>
<body>
<div class="container">
  <section id="content">
    <?php echo form_open_multipart('Users/add'); ?>
	<h1>Register Form</h1>
	<div><input type="text" placeholder="Username" name="username" id="username" value="<?php echo set_value('username');?>"/></div>
    <div class="error"><?php echo form_error('username'); ?></div>
	<div><input type="password" placeholder="Password" name="password" id="username"/></div>
    <div class="error"><?php echo form_error('password'); ?></div>
	<div><input type="text" placeholder="Full Name" name="name" id="username" value="<?php echo set_value('name');?>" /></div>
    <div class="error"><?php echo form_error('name'); ?></div>
	<div><input type="text" placeholder="Email" name="email" id="username" value="<?php echo set_value('email');?>"/></div>
    <div class="error"><?php echo form_error('email'); ?></div>
	<div><?=$image;?></div>
    <div><input type="text" placeholder="Enter Captcha" name="token" id="password"/></div>
    <div class="error"><?=$captcha; ?></div>
	<div><input type="submit" value="Register" />
	  <a href="<?php echo base_url();?>">Login</a>
	</div>
    <?php	echo form_close(); ?>
  </section><!-- content -->
</div></body>
</html>

