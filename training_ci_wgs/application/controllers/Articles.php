<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Articles extends CI_Controller {

  public function __construct(){
    parent::__construct();
	$this->load->model('Article');	
  }

  public function add(){
	if($this->check_login()){
      $this->load->library('form_validation');
	  $this->form_validation->set_rules('title', 'Title article', 'trim|required');
	  $this->form_validation->set_rules('content', 'Content article', 'trim|required');

	  if(($this->form_validation->run() == FALSE)){
        $this->load->view('Articles/add_article');
	  } else {			
	    if($query = $this->Article->add()){
		  $this->view();
	    } else {
		  $this->load->view('Articles/add_article');
	    }
	  }
	} else {
	  $this->load->view('User/login');
	}
  }

  public function view($id=NULL){
	if($this->check_login()){
	  $jml = $this->db->get('article');
      //setting pagination
      $config['base_url'] = base_url().'Articles/view';
      $config['total_rows'] = $jml->num_rows();
      $config['per_page'] = '5';
      $config['first_page'] = 'Prev';
      $config['last_page'] = 'Next';
      $config['next_page'] = '&laquo;';
      $config['prev_page'] = '&raquo;';

      $this->pagination->initialize($config);
      $data['page'] = $this->pagination->create_links();
	  $data['data']= $this->Article->view($config['per_page'], $id);	
	  $this->load->view('Articles/view_article',$data);		
	} else {
	  $this->load->view('User/login');
	}
  }

  public function edit() { 
	if($this->check_login()){
      $this->load->library('form_validation');
	  // field name, error message, validation rules
	  $this->form_validation->set_rules('title', 'Title article', 'trim|required');
	  $this->form_validation->set_rules('content', 'Content article', 'trim|required');

	  if(($this->form_validation->run() == FALSE)){
        $id_article=$this->uri->segment(3); 
	    $data['data']=$this->Article->per_id($id_article);
	    $this->load->view('Articles/edit_article',$data);
	  } else {			
	    if($query = $this->Article->edit()){
	      $message = "Article was edited";
	  	  echo "<script>javascript:alert('".$message."');</script>";
		  $this->view();
	    } else {
          $id_article=$this->uri->segment(3); 
	      $data['data']=$this->Article->per_id($id_article);
          $this->load->view('Articles/edit_article',$data);
	    }
	  }
	} else {
	  $this->load->view('User/login');
	}
  }

  public function delete() { 
	if($this->check_login()){
  	  $id_article=$this->uri->segment(3); 
	  $this->Article->delete($id_article); 
	  $message = "Article was deleted";
	  echo "<script>javascript:alert('".$message."');</script>";
	  $this->view();
	} else {
	  $this->load->view('User/login');
	}
  }

  private function check_login(){
	$login_status = $this->session->userdata('login_status');
	if(!isset($login_status) || $login_status != true){
	  return false;
	} else {
	  return true;
	}  
  }

}
